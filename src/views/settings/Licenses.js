import React, {useState, useEffect} from 'react';
import {ScrollView, StyleSheet, FlatList} from 'react-native';
import {withTheme, Divider, List} from 'react-native-paper';
import {useHistory, useRouteMatch} from 'react-router-native';
import {withTitle} from '@components/Title';
import Markdown from 'react-native-markdown-renderer';

const Licenses = props => {
  const [licenses, setLicenses] = useState({});
  const history = useHistory();

  useEffect(() => {
    import('../../../licenses.json').then(setLicenses);
  }, []);

  props.setTitle('Third-Party Licenses');
  return (
    <FlatList
      data={Object.keys(licenses)}
      keyExtractor={item => item}
      ItemSeparatorComponent={Divider}
      renderItem={({item, index}) => {
        const l = licenses[item];
        return (
          <List.Item
            title={`${l.name}`}
            description={l.publisher}
            onPress={() => {
              console.log(item);
              history.push(`/settings/license/${encodeURIComponent(item)}`);
            }}
          />
        );
      }}
    />
  );
};

export default withTitle(Licenses);
