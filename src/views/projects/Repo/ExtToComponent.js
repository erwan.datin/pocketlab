import React, {useState, useEffect} from 'react';
import {Image} from 'react-native';
import SyntaxHighlighter from 'react-native-syntax-highlighter';
import {atomOneLight} from 'react-syntax-highlighter/styles/hljs';
import {decode} from 'base-64';
import Markdown from 'react-native-markdown-renderer';

const ExtToComponent = ({extension, data}) => {
  let component = <></>;
  switch (extension.toLowerCase()) {
    case 'jpg':
    case 'jpeg':
    case 'png':
    case 'gif':
    case 'bmp':
      component = (
        <Image
          style={{width: '100%', height: '100%'}}
          resizeMode="center"
          source={{uri: `data:image/${extension};base64,${data}`}}
        />
      );
      break;
    case 'md':
      component = <Markdown>{decode(data)}</Markdown>;
      break;
    default:
      component = (
        <SyntaxHighlighter style={atomOneLight}>
          {decode(data)}
        </SyntaxHighlighter>
      );
      break;
  }
  return component;
};

export default ExtToComponent;
