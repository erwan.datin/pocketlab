import React, {useState, useEffect} from 'react';
import {StyleSheet, FlatList} from 'react-native';
import {List, ActivityIndicator, Divider} from 'react-native-paper';
import {withRouter} from 'react-router-native';
import GitLabAPI from '../../../GitLabAPI';

const ViewTree = props => {
  const {params} = props.match;
  const [path, setPath] = useState('');
  const [items, setItems] = useState(null);

  useEffect(() => {
    const gitlab = new GitLabAPI();
    gitlab.repository(params.project, {path}).then(response => {
      if (Array.isArray(response)) {
        if (path !== '') {
          response = [{name: '..', type: 'tree', id: 0}, ...response];
        }
      } else {
        response = [];
      }
      setItems(response);
    });
  }, [params.project, path]);

  if (items === null) {
    return <ActivityIndicator animating={true} size="large" />;
  }

  return (
    <FlatList
      data={items}
      ItemSeparatorComponent={Divider}
      renderItem={({item}) => {
        return (
          <List.Item
            key={item.id.toString()}
            title={`${item.name}`}
            onPress={() => {
              if (item.type === 'tree') {
                if (item.name === '..') {
                  const parts = path.split('/');
                  parts.splice(parts.length - 1, 1);
                  const parent = parts.join('/');
                  setPath(parent);
                } else {
                  setPath(path + (path !== '' ? '/' : '') + item.name);
                }
              } else if (item.type === 'blob') {
                const parts = item.name.split('.');

                props.history.push(
                  `/projects/${params.project}/view-blob/${item.id}?ext=${
                    parts[parts.length - 1]
                  }`,
                );
              }
            }}
            left={leftProps => (
              <List.Icon
                {...leftProps}
                icon={item.type === 'tree' ? 'folder' : 'file'}
              />
            )}
          />
        );
      }}
    />
  );
};

export default withRouter(ViewTree);
