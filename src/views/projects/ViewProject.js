import React, {useEffect} from 'react';
import {Headline, BottomNavigation} from 'react-native-paper';
import GitLabAPI from '../../GitLabAPI';
import {withTitle} from '@components/Title';
import ViewTree from './Repo/ViewTree';
import ListIssues from './Issues/List';
import ListPipelines from './Pipelines/List';
import ListMergeRequests from './MergeRequests/List';
import Overview from './Overview';

const routes = [
  {key: 'overview', title: 'Overview', icon: 'home'},
  {key: 'repo', title: 'Repository', icon: 'source-repository'},
  {key: 'issues', title: 'Issues', icon: 'cards-outline'},
  {key: 'mergeRequests', title: 'Merge Requests', icon: 'source-merge'},
  {key: 'cicd', title: 'CI / CD', icon: 'rocket'},
];

const renderScene = BottomNavigation.SceneMap({
  overview: () => <Overview />,
  repo: () => <ViewTree />,
  issues: () => <ListIssues />,
  mergeRequests: () => <ListMergeRequests />,
  cicd: () => <ListPipelines />,
});

const ViewProject = props => {
  const {project, tab} = props.match.params;
  const selectedTab = routes.findIndex(element => element.key === tab);

  useEffect(() => {
    const gitlab = new GitLabAPI();
    gitlab.project(project).then(response => {
      props.setTitle(`${response.name_with_namespace}`);
    });
  }, [project, props]);

  return (
    <BottomNavigation
      navigationState={{index: selectedTab, routes}}
      onIndexChange={newIndex =>
        props.history.replace(`/projects/${project}/${routes[newIndex].key}`)
      }
      renderScene={renderScene}
    />
  );
};

export default withTitle(ViewProject);
