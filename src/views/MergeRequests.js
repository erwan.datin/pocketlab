import React, {useState, useEffect} from 'react';
import {StyleSheet, Image, ScrollView, View, FlatList} from 'react-native';
import {withRouter} from 'react-router-native';
import GitLabAPI from '../GitLabAPI';
import MergeRequestList from '@components/MergeRequestList';
import {withTitle} from '@components/Title';

const ListMergeRequests = props => {
  const [mergeRequests, setMergeRequests] = useState(null);
  const [filter, setFilter] = useState('opened');

  useEffect(() => {
    props.setTitle('Merge Requests');
    const gitlab = new GitLabAPI();
    gitlab
      .mergeRequests({scope: 'assigned_to_me', state: filter})
      .then(response => {
        if (!Array.isArray(response)) {
          response = [];
        }
        setMergeRequests(response);
      });
  }, [filter, props]);

  return (
    <MergeRequestList
      currentFilter={filter}
      onCurrentFilterChanged={setFilter}
      mergeRequests={mergeRequests}
    />
  );
};

export default withTitle(withRouter(ListMergeRequests));
