import React, {useState, useEffect} from 'react';
import {StyleSheet, ScrollView, View, Text, FlatList} from 'react-native';
import {
  Avatar,
  Button,
  List,
  Title,
  Paragraph,
  ActivityIndicator,
} from 'react-native-paper';
import {withRouter} from 'react-router-native';
import moment from 'moment';
import GitLabAPI from '../GitLabAPI';
import {withTitle} from '@components/Title';
import Illustration from '@gitlab/svgs/dist/illustrations/todos_all_done.svg';

const styles = StyleSheet.create({
  todoDone: {
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
  listContainer: {
    flex: 1,
    alignItems: 'center',
  },
  emptyListContainer: {
    flex: 1,
    width: '100%',
    flexDirection: 'column',
    alignItems: 'center',
  },
});

const todoTypes = {
  Issue: 'issues',
  MergeRequest: 'merge_request',
};

const Todos = props => {
  const [todos, setTodos] = useState(null);

  const loadTodos = () => {
    const gitlab = new GitLabAPI();
    gitlab.todos().then(response => {
      if (Array.isArray(response)) {
        setTodos(response);
      } else {
        setTodos([]);
      }
    });
  };

  props.setTitle('Todos');
  useEffect(() => {
    loadTodos();
  }, []);

  if (todos === null) {
    return <ActivityIndicator animating={true} size="large" />;
  }

  return (
    <FlatList
      data={todos}
      keyExtractor={item => item.id}
      onRefresh={loadTodos}
      refreshing={false}
      contentContainerStyle={todos.length === 0 ? styles.listContainer : {}}
      //contentContainerStyle={styles.listContainer}
      ListEmptyComponent={() => (
        <View style={styles.emptyListContainer}>
          <Illustration width="90%" height="80%" />
          <Title>Congrats! You've got nothing left to do</Title>
        </View>
      )}
      renderItem={({item, index}) => {
        const markAsDone = () => {
          const gitlab = new GitLabAPI();
          gitlab.markTodoAsDone(item.id);
          // React doesn't re-render if you update todos and
          // then call setTodos(todos)
          const newTodos = [...todos];
          newTodos[index].state = 'done';
          setTodos(newTodos);
        };

        let button = <></>;
        if (item.state != 'done') {
          button = <Button onPress={markAsDone}>Done</Button>;
        }

        const onPress = () => {
          if (todoTypes[item.target_type] !== undefined) {
            const issueUrl = `/projects/${item.project.id}/${
              todoTypes[item.target_type]
            }/${item.target.iid}`;
            props.history.push(issueUrl);
          }
        };

        const when = moment(item.created_at).fromNow();
        return (
          <List.Item
            key={item.id.toString()}
            title={`${item.author.name} ${item.action_name} ${item.body}`}
            description={when}
            titleNumberOfLines={3}
            titleStyle={item.state == 'done' ? styles.todoDone : {}}
            onPress={onPress}
            right={props => button}
          />
        );
      }}
    />
  );
};

export default withTitle(withRouter(Todos));
